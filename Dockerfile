FROM ruby:2.3.3
RUN apt-get update -qq && apt-get install -y build-essential nodejs mysql-client
RUN mkdir /trans-info
WORKDIR /trans-info
ADD Gemfile /trans-info/Gemfile
ADD Gemfile.lock /trans-info/Gemfile.lock
RUN bundle install
ADD . /trans-info