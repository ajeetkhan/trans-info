Controller : plural
example : rails g controller clients

Model : Singular with first word capitalized
example : Client, LineItem, Mouse

Table/schema : small letter, words separated by "_" and plural
example : articles, line_items, mice

Foreign Keys : singularized_table_name_id
example : item_id, order_id

color theme - blue(#007ACC)

To do:
1. On login remove unwanted notice like signed out successfully
2. change color scheme. 
3. Login heading looks like button(change the look)
4. Create header and footer. (Completed)
5. Add proper links on the home page under all sidemenu item. (completed)
6. Remove "Organization profile" and "staff" link from header of home page.
7. Show organization profile details on index page in a well formatted way. Provide links to edit the profile. Remove Destroy and Show link on the same
8. Remove Show link on the edit page of organization.(completed)
9. Increase the width of the panel for content on each page except login and signup.
10. Apply bootstrap on index page of students. Show necessary details. (completed)
11. Apply bootstrap on index page of staffs. Show necessary details. (completed)
12. Make sticky footer. (completed)
13. Change the UI of the send staff message page. (completed)
14. Organization index page should not be their. 
15. Staff edit page not working.
16. Edit profile link not working.
17. In every show page we have to change the back link from index to create new.


1. disable sent_to textbox in message page.
2. use google translator api for hindi message.
3. 






Navbar code for user loggedin and organization profile edit link
<% if user_signed_in? %>
  Logged in as <strong><%= current_user.email %></strong>.
  <%= link_to 'Edit profile', edit_user_registration_path, :class => 'navbar-link' %> |
  <%= link_to "Logout", destroy_user_session_path, method: :delete, :class => 'navbar-link'  %>
<% else %>
  <%= link_to "Sign up", new_user_registration_path, :class => 'navbar-link'  %> |
  <%= link_to "Login", new_user_session_path, :class => 'navbar-link'  %>
<% end %>
<br>
<% @org.each do |o| %>
<% if @org.empty? %>
	 <%= link_to "Add Organization", new_organization_path, :class => 'navbar-link'  %> |
<% else %>
	
	<%= link_to "Organization Profile", organization_path(o), :class => 'navbar-link'  %>
<% end %>
<% end %>
<%= link_to "Staff", new_staff_path, :class => 'navbar-link'  %>


Quick link code for staff_message

