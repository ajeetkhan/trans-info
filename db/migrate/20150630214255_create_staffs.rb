class CreateStaffs < ActiveRecord::Migration
  def change
    create_table :staffs do |t|

	    t.string   "name",            limit: 255
	    t.string   "designation",     limit: 255
	    t.text     "address",         limit: 65535
	    t.string   "contact_no",      limit: 255
	    t.string   "email",           limit: 255
	    t.string   "staff_type",      limit: 255
	    t.datetime "created_at",                    null: false
	    t.datetime "updated_at",                    null: false
	    t.integer  "organization_id", limit: 4
  	end

  	add_index "staffs", ["organization_id"], name: "index_staffs_on_organization_id", using: :btree


    end
end
