class AddOrganizationIdToStudents < ActiveRecord::Migration
  def change
  	add_column :students, :organization_id, :integer
    add_index :students, :organization_id
  end
end
