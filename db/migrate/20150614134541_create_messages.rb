class CreateMessages < ActiveRecord::Migration
  def change
    create_table :messages do |t|
      t.text :message_text
      t.string :sent_to

      t.timestamps null: false
    end
  end
end
