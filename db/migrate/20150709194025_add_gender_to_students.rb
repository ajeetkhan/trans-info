class AddGenderToStudents < ActiveRecord::Migration
  def change
  	add_column :students, :gender, :string
  	add_column :students, :date_of_birth, :date
  end
end
