class CreateOrganizations < ActiveRecord::Migration
  def change
    create_table :organizations do |t|
      t.integer :registration_no
      t.string :owner_name
      t.string :organization_name
      t.text :address
      t.integer :contact_no
      t.integer :additional_contact_no

      t.timestamps null: false
    end
  end
end
