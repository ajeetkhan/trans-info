class AddUserTypeToUsers < ActiveRecord::Migration
  def change
    add_column :users, :user_type, :string, default: 'useradmin'
    add_column :users, :allow, :boolean, default: 0
  end
end
