class CreateStudents < ActiveRecord::Migration
  def change
    create_table :students do |t|
      t.string :enrollment_no
      t.string :roll_no
      t.text :address
      t.string :father_name
      t.string :mother_name
      t.string :parents_contact_no
      t.string :additional_contact_no
      t.string :parents_email
      t.string :student_contact_no
      t.boolean :alumni

      t.timestamps null: false
    end
  end
end
