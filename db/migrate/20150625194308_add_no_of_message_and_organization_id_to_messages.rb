class AddNoOfMessageAndOrganizationIdToMessages < ActiveRecord::Migration
  def change
    add_column :messages, :no_of_message, :integer
    add_column :messages, :organization_id, :integer
    add_index :messages, :organization_id
  end
end
