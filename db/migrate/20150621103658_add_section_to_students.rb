class AddSectionToStudents < ActiveRecord::Migration
  def change
    add_column :students, :section, :string
  end
end
