class AddPromoLimitAndTransLiitToOrganizations < ActiveRecord::Migration
  def change
  	add_column :organizations, :promo_limit, :string, default: '0'
  	add_column :organizations, :trans_limit, :string, default: '0'
  end
end
