class ChangeDatatypeInOrganizationTable < ActiveRecord::Migration
  def change
  	change_column :organizations, :registration_no, :string
  	change_column :organizations, :contact_no, :string
  	change_column :organizations, :additional_contact_no, :string
  end
end
