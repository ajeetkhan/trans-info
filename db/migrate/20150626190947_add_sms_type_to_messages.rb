class AddSmsTypeToMessages < ActiveRecord::Migration
  def change
    add_column :messages, :sms_type, :string
  end
end
