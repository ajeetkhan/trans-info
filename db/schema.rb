# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150709194025) do

  create_table "messages", force: :cascade do |t|
    t.text     "message_text",    limit: 65535
    t.string   "sent_to",         limit: 255
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.integer  "no_of_message",   limit: 4
    t.integer  "organization_id", limit: 4
    t.string   "sms_type",        limit: 255
    t.string   "sendto",          limit: 255
  end

  add_index "messages", ["organization_id"], name: "index_messages_on_organization_id", using: :btree

  create_table "organizations", force: :cascade do |t|
    t.string   "registration_no",       limit: 255
    t.string   "owner_name",            limit: 255
    t.string   "organization_name",     limit: 255
    t.text     "address",               limit: 65535
    t.string   "contact_no",            limit: 255
    t.string   "additional_contact_no", limit: 255
    t.datetime "created_at",                                        null: false
    t.datetime "updated_at",                                        null: false
    t.integer  "user_id",               limit: 4
    t.string   "promo_limit",           limit: 255,   default: "0"
    t.string   "trans_limit",           limit: 255,   default: "0"
  end

  create_table "staffs", force: :cascade do |t|
    t.string   "name",            limit: 255
    t.string   "designation",     limit: 255
    t.text     "address",         limit: 65535
    t.string   "contact_no",      limit: 255
    t.string   "email",           limit: 255
    t.string   "staff_type",      limit: 255
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.integer  "organization_id", limit: 4
  end

  add_index "staffs", ["organization_id"], name: "index_staffs_on_organization_id", using: :btree

  create_table "students", force: :cascade do |t|
    t.string   "enrollment_no",         limit: 255
    t.string   "roll_no",               limit: 255
    t.text     "address",               limit: 65535
    t.string   "father_name",           limit: 255
    t.string   "mother_name",           limit: 255
    t.string   "parents_contact_no",    limit: 255
    t.string   "additional_contact_no", limit: 255
    t.string   "parents_email",         limit: 255
    t.string   "student_contact_no",    limit: 255
    t.boolean  "alumni",                limit: 1
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.integer  "organization_id",       limit: 4
    t.string   "class_name",            limit: 255
    t.string   "section",               limit: 255
    t.string   "name",                  limit: 255
    t.string   "gender",                limit: 255
    t.date     "date_of_birth"
  end

  add_index "students", ["organization_id"], name: "index_students_on_organization_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "email",               limit: 255, default: "",          null: false
    t.string   "encrypted_password",  limit: 255, default: "",          null: false
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",       limit: 4,   default: 0,           null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",  limit: 255
    t.string   "last_sign_in_ip",     limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "user_type",           limit: 255, default: "useradmin"
    t.boolean  "allow",               limit: 1,   default: false
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree

end
