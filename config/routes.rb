Rails.application.routes.draw do

  get "messages/staff_message" => 'messages#staff_message', as: "staff_message"
  get "messages/all_student_message" => 'messages#all_student_message', as: "all_student_message"

  get "students/update_student_list" => 'students#update_student_list', as: "update_student_list"
  get "students/update_student_list_class" => 'students#update_student_list_class', as: "update_student_list_class"
  resources :students do
    collection { post :import }
  end  

  resources :staffs
  
  get "messages/update_contact_list" => 'messages#update_contact_list', as: "update_contact_list"
  get "messages/update_contact_list_class" => 'messages#update_contact_list_class', as: "update_contact_list_class"
  get "messages/update_staf_list" => 'messages#update_staff_list', as: "update_staff_list"
  get "messages/show_all_student_contact_list" => 'messages#show_all_student_contact_list', as: "show_all_student_contact_list"

  resources :messages

  resources :organizations

  get 'admin/index'

  devise_for :users
  get 'home/index'

  devise_scope :user do
    match '/sign-in' => "devise/sessions#new",:via => [:get], :as => :login
  end
  
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"

  root to: 'home#index'
  
  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
