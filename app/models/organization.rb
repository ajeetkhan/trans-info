class Organization < ActiveRecord::Base
	belongs_to :user, :class_name => 'User', :foreign_key => 'user_id'
	has_many :staffs, dependent: :destroy
	has_many :students, dependent: :destroy
	has_many :messages
	
end
