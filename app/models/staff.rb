class Staff < ActiveRecord::Base
	belongs_to :orgaization, :class_name => 'orgaization', :foreign_key => 'organization_id'
	STAFF_TYPE = ["None","Teaching", "Non-Teaching", "Clerical"]
end
