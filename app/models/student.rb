class Student < ActiveRecord::Base
	
	require 'roo'

	belongs_to :orgaization, :class_name => 'User', :foreign_key => 'user_id'
	
	CLASS_LIST = ["None", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"]
	SECTION_LIST = ["None", "A", "B", "C", "D", "E", "PCM", "PCB", "COM+MATHS", "COM-A", "COM-B", "COM-M"]
	GENDER_LIST = ["Male", "Female", "Others"]
	
	
=begin
def self.import(file, current_organization_id)
		
		CSV.foreach(file.path, headers: true) do |row|	
	      record = Student.find_by(:organization_id => current_organization_id,:enrollment_no => row["enrollment_no"]) || new
	      record.organization_id= current_organization_id
	      record.attributes = row.to_hash.slice(*row.to_hash.keys)
		  record.save!
	    end
	end	=end
=end


def self.import(file, current_organization_id)
  spreadsheet = open_spreadsheet(file)
  header = spreadsheet.row(1)
	# raise spreadsheet.to_yaml
  (2..spreadsheet.last_row).each do |i|
    row = Hash[[header, spreadsheet.row(i)].transpose]
		# raise record.to_yaml
    data = Student.where(:organization_id => current_organization_id,:enrollment_no => row["enrollment_no"].to_i)
		unless data.present?
			record = Student.new
			record.organization_id= current_organization_id
			if row["enrollment_no"].present?
				record.enrollment_no = row["enrollment_no"].to_i
			end
			if row["roll_no"].present?
				record.roll_no = row["roll_no"].to_i
			end
			if row["student_contact_no"].present?
				student_contact_no = row["student_contact_no"].to_i.to_s
				if (student_contact_no.length == 10) && (student_contact_no[0] == "7" || student_contact_no[0] == "8" || student_contact_no[0] == "9")
					record.student_contact_no = row["student_contact_no"].to_i
				end	
			end	
			if row["parents_contact_no"].present?
				parents_contact_no = row["parents_contact_no"].to_i.to_s
				if (parents_contact_no.length == 10) && (parents_contact_no[0] == "7" || parents_contact_no[0] == "8" || parents_contact_no[0] == "9")
					record.parents_contact_no = row["parents_contact_no"].to_i
				end
			end
			if row["address"].present?
				record.address = row["address"]
			end
			if row["father_name"].present?
				record.father_name = row["father_name"]
			end
			if row["mother_name"].present?
				record.mother_name = row["mother_name"]
			end
			if row["name"].present?
				record.name = row["name"]
			end
			if row["class_name"].present?
				record.class_name = row["class_name"]
			end
			if row["section"].present?
				record.section = row["section"]
			end
			if row["gender"].present?
				record.gender = row["gender"]
			end
			if row["date_of_birth"].present?
				record.date_of_birth = row["date_of_birth"]
			end
			record.save!	
		end
  end
end


def self.open_spreadsheet(file)
  case File.extname(file.original_filename)
  when ".csv" then Roo::CSV.new(file.path)
  when ".xls" then Roo::Excel.new(file.path)
  when ".xlsx" then Roo::Excelx.new(file.path)
  else raise "Unknown file type: #{file.original_filename}"
  end
end
	
end
