class StudentsController < ApplicationController
  before_action :set_student, only: [:show, :edit, :update, :destroy]
  before_filter :authenticate_user!
  layout "user_layout"
  # GET /students
  # GET /students.json
  def index
    @students = Student.where(organization_id: session[:current_organization_id])
  end

  # GET /students/1
  # GET /students/1.json
  def show
    
    @student= Student.find(params[:id])

  end

  # GET /students/new
  def new
    @student = Student.new
  end

  # GET /students/1/edit
  def edit
    @student = Student.find(params[:id])
  end

  def update_student_list
    
    @student_with_class = Student.where(class_name: params[:c_name], section: params[:s_name], organization_id: session[:current_organization_id])
    respond_to do |format|
      format.js 
      format.html
    end  
  end

  def update_student_list_class
    
    @student_with_class = Student.where(class_name: params[:c_name], organization_id: session[:current_organization_id])
    respond_to do |format|
      format.js 
      format.html
    end  
  end

  # POST /students
  # POST /students.json
  def create
    @student = Student.new(student_params)

    respond_to do |format|
      if @student.save
        format.html { redirect_to @student, notice: 'Student was successfully created.' }
        format.json { render :show, status: :created, location: @student }
      else
        format.html { render :new }
        format.json { render json: @student.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /students/1
  # PATCH/PUT /students/1.json
  def update
    @student= Student.find(params[:id])
    respond_to do |format|
      if @student.update(student_params)
        format.html { redirect_to @student, notice: 'Student was successfully updated.' }
        format.json { render :show, status: :ok, location: @student }
      else
        format.html { render :edit }
        format.json { render json: @student.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /students/1
  # DELETE /students/1.json
  def destroy
    @student= Student.find(params[:id])
    @student.destroy
    respond_to do |format|
      format.html { redirect_to students_url, notice: 'Student was successfully destroyed.' }
      format.json { head :no_content }
    end
  end


  def import
    
    Student.import(params[:file], session[:current_organization_id])
    #puts @session[:current_organization_id].inspect
    redirect_to students_path, notice: "Record imported Successfully."
  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_student
      @student = Student.where(organization_id: session[:current_organization_id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def student_params
      params.require(:student).permit(:enrollment_no, :roll_no, :address, :father_name, :mother_name, :parents_contact_no, :additional_contact_no, :parents_email, :student_contact_no, :alumni, :organization_id, :class_name, :section, :name, :gender, :date_of_birth)
    end

  
end
