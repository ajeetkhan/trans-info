class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  protected
  def authenticate_user!
    #authenticating if the user is loggedin
    if user_signed_in?
      super
    else
      redirect_to login_path, :notice => 'Please Sign in to continue...'
      ## if you want render 404 page
      ## render :file => File.join(Rails.root, 'public/404'), :formats => [:html], :status => 404, :layout => false
    end
    # authenticating if the user has rights to access the page after login
    @org_allow = User.where(id: current_user.id, allow: 0)
    @org_allow.each do |allowed|
      if !(allowed.allow?)
        redirect_to home_index_path, :notice => 'You do not have sufficient permission, Get your account activated!'
      end
    end
  end
end
