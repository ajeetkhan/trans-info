class HomeController < ApplicationController
  
  def index
  	if user_signed_in?
  		
  		@user = User.where(id: current_user.id).first
      @allowed_user = @user.allow
        
  		@org = Organization.where(user_id: current_user.id).first
  		#render :template => 'organizations/show'
      if @org.present?
        session[:current_organization_id] = @org.id
      end
  		respond_to do |format|
  			format.html { render 'admin/index', layout: "user_layout" }
  			format.json { render json:@org}
  		end	
  	end
  end
end
