class AdminController < ApplicationController
	
	before_filter :authenticate_user!
	layout "user_layout"
  def index
  	render layout: "user_layout"
  end
  
end
