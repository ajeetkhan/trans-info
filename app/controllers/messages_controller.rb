# encoding: utf-8
class MessagesController < ApplicationController
  
  before_action :set_message, only: [:show, :edit, :update, :destroy]
  before_filter :authenticate_user!
  layout "user_layout"
  def staff_message
    @organization = Organization.where(user_id: current_user.id).first
    @message = Message.new
  end

  def all_student_message
    @organization = Organization.where(user_id: current_user.id).first
    @message = Message.new
  end

  # GET /messages
  # GET /messages.json
  def index
    @org_msg_limit = Organization.where(user_id: current_user.id)
    @messages = Message.where(organization_id: session[:current_organization_id])
  end

  # GET /messages/1
  # GET /messages/1.json
  def show
  end

  # GET /messages/new
  def new
    @organization = Organization.where(user_id: current_user.id).first
    @message = Message.new
  end

  # GET /messages/1/edit
  def edit
  end

  def update_staff_list
        @staff_members = Staff.where(staff_type: params[:s_type], organization_id: session[:current_organization_id])
        respond_to do |format|
          format.js
        end
  end

  def update_contact_list
    @contact = Student.where(class_name: params[:c_name], section: params[:s_name], organization_id: session[:current_organization_id])
    #print @contact.map(&:parents_contact_no)
    respond_to do |format|
      format.js 
    end
  end

  def update_contact_list_class
    @contact = Student.where(class_name: params[:c_name], organization_id: session[:current_organization_id])
    respond_to do |format|
      format.js 
    end
  end

  def show_all_student_contact_list
    section_name = params[:s_name]
    class_name = params[:c_name]
    if class_name == "None"  
      @contact = Student.where(organization_id: session[:current_organization_id])
    else
      if section_name == "None"
        @contact = Student.where(class_name: class_name, organization_id: session[:current_organization_id])
      else
         @contact = Student.where(class_name: class_name, section: section_name, organization_id: session[:current_organization_id])
      end   
    end 
    respond_to do |format|
      format.js 
    end
  end


  # POST /messages
  # POST /messages.json
  def create
    @message = Message.new(message_params)
    numbers = @message.sent_to.try(:split,",")
    @message.no_of_message = numbers.try(:count)
    if message_params[:sendto]=""
      @message.sendto = params[:sendto]
    end
    @message.sent_to = @message.sent_to
    org =Organization.find(session[:current_organization_id])
    respond_to do |format|
      if @message.save
        if message_params[:sms_type] == "TRANS"
          org.trans_limit = org.trans_limit.to_i - @message.no_of_message.to_i
          encoded_url = URI.encode("http://sms.hspsms.com/sendSMS?username=nitesh.mishra143@gmail.com&message="+@message.message_text+"&sendername=EXCEED&smstype="+@message.sms_type+"&numbers="+numbers.join(',')+"&apikey=eaf93dbe-bf68-43c7-8e47-c7a38bae0371")
          uri = URI.parse(encoded_url)
          Net::HTTP.get(uri)
        elsif message_params[:sms_type] == "PROMO"
          #same thing for other business logic
          org.promo_limit = org.promo_limit.to_i - @message.no_of_message.to_i
          encoded_url = URI.encode("http://sms.hspsms.com/sendSMS?username=nitesh.mishra143@gmail.com&message="+@message.message_text+"&sendername=EXCEED&smstype="+@message.sms_type+"&numbers="+numbers.join(',')+"&apikey=eaf93dbe-bf68-43c7-8e47-c7a38bae0371")
          uri = URI.parse(encoded_url)
          Net::HTTP.get(uri) 
          # puts(org.promo_limit)
        end
        org.save!
        format.html { redirect_to @message, notice: 'Message was successfully created.' }
      end
    end

  end

  # PATCH/PUT /messages/1
  # PATCH/PUT /messages/1.json
  def update
    respond_to do |format|
      if @message.update(message_params)
        format.html { redirect_to @message, notice: 'Message was successfully updated.' }
        format.json { render :show, status: :ok, location: @message }
      else
        format.html { render :edit }
        format.json { render json: @message.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /messages/1
  # DELETE /messages/1.json
  def destroy
    @message.destroy
    respond_to do |format|
      format.html { redirect_to messages_url, notice: 'Message was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_message
      @message = Message.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def message_params
      params.require(:message).permit(:message_text, :sent_to, :no_of_message, :organization_id, :sms_type, :staff_type, :sendto, :transliterateTextarea)
    end
end
