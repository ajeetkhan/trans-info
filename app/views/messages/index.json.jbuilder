json.array!(@messages) do |message|
  json.extract! message, :id, :message_text, :sent_to
  json.url message_url(message, format: :json)
end
