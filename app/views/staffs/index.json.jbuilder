json.array!(@staffs) do |staff|
  json.extract! staff, :id, :name, :designation, :address, :contact_no, :email, :staff_type
  json.url staff_url(staff, format: :json)
end
