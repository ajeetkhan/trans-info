json.array!(@students) do |student|
  json.extract! student, :id, :enrollment_no, :roll_no, :address, :father_name, :mother_name, :parents_contact_no, :additional_contact_no, :parents_email, :student_contact_no, :alumni
  json.url student_url(student, format: :json)
end
