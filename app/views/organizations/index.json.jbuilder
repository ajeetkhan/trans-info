json.array!(@organizations) do |organization|
  json.extract! organization, :id, :registration_no, :owner_name, :organization_name, :address, :contact_no, :additional_contact_no
  json.url organization_url(organization, format: :json)
end
