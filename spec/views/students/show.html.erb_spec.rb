require 'spec_helper'

describe "students/show" do
  before(:each) do
    @student = assign(:student, stub_model(Student,
      :enrollment_no => "Enrollment No",
      :roll_no => "Roll No",
      :address => "MyText",
      :father_name => "Father Name",
      :mother_name => "Mother Name",
      :parents_contact_no => "Parents Contact No",
      :additional_contact_no => "Additional Contact No",
      :parents_email => "Parents Email",
      :student_contact_no => "Student Contact No",
      :alumni => false
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Enrollment No/)
    rendered.should match(/Roll No/)
    rendered.should match(/MyText/)
    rendered.should match(/Father Name/)
    rendered.should match(/Mother Name/)
    rendered.should match(/Parents Contact No/)
    rendered.should match(/Additional Contact No/)
    rendered.should match(/Parents Email/)
    rendered.should match(/Student Contact No/)
    rendered.should match(/false/)
  end
end
