require 'spec_helper'

describe "students/new" do
  before(:each) do
    assign(:student, stub_model(Student,
      :enrollment_no => "MyString",
      :roll_no => "MyString",
      :address => "MyText",
      :father_name => "MyString",
      :mother_name => "MyString",
      :parents_contact_no => "MyString",
      :additional_contact_no => "MyString",
      :parents_email => "MyString",
      :student_contact_no => "MyString",
      :alumni => false
    ).as_new_record)
  end

  it "renders new student form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", students_path, "post" do
      assert_select "input#student_enrollment_no[name=?]", "student[enrollment_no]"
      assert_select "input#student_roll_no[name=?]", "student[roll_no]"
      assert_select "textarea#student_address[name=?]", "student[address]"
      assert_select "input#student_father_name[name=?]", "student[father_name]"
      assert_select "input#student_mother_name[name=?]", "student[mother_name]"
      assert_select "input#student_parents_contact_no[name=?]", "student[parents_contact_no]"
      assert_select "input#student_additional_contact_no[name=?]", "student[additional_contact_no]"
      assert_select "input#student_parents_email[name=?]", "student[parents_email]"
      assert_select "input#student_student_contact_no[name=?]", "student[student_contact_no]"
      assert_select "input#student_alumni[name=?]", "student[alumni]"
    end
  end
end
