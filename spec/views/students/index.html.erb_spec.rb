require 'spec_helper'

describe "students/index" do
  before(:each) do
    assign(:students, [
      stub_model(Student,
        :enrollment_no => "Enrollment No",
        :roll_no => "Roll No",
        :address => "MyText",
        :father_name => "Father Name",
        :mother_name => "Mother Name",
        :parents_contact_no => "Parents Contact No",
        :additional_contact_no => "Additional Contact No",
        :parents_email => "Parents Email",
        :student_contact_no => "Student Contact No",
        :alumni => false
      ),
      stub_model(Student,
        :enrollment_no => "Enrollment No",
        :roll_no => "Roll No",
        :address => "MyText",
        :father_name => "Father Name",
        :mother_name => "Mother Name",
        :parents_contact_no => "Parents Contact No",
        :additional_contact_no => "Additional Contact No",
        :parents_email => "Parents Email",
        :student_contact_no => "Student Contact No",
        :alumni => false
      )
    ])
  end

  it "renders a list of students" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Enrollment No".to_s, :count => 2
    assert_select "tr>td", :text => "Roll No".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => "Father Name".to_s, :count => 2
    assert_select "tr>td", :text => "Mother Name".to_s, :count => 2
    assert_select "tr>td", :text => "Parents Contact No".to_s, :count => 2
    assert_select "tr>td", :text => "Additional Contact No".to_s, :count => 2
    assert_select "tr>td", :text => "Parents Email".to_s, :count => 2
    assert_select "tr>td", :text => "Student Contact No".to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
  end
end
