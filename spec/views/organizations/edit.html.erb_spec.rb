require 'spec_helper'

describe "organizations/edit" do
  before(:each) do
    @organization = assign(:organization, stub_model(Organization,
      :registration_no => 1,
      :owner_name => "MyString",
      :organization_name => "MyString",
      :address => "MyText",
      :contact_no => 1,
      :additional_contact_no => 1
    ))
  end

  it "renders the edit organization form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", organization_path(@organization), "post" do
      assert_select "input#organization_registration_no[name=?]", "organization[registration_no]"
      assert_select "input#organization_owner_name[name=?]", "organization[owner_name]"
      assert_select "input#organization_organization_name[name=?]", "organization[organization_name]"
      assert_select "textarea#organization_address[name=?]", "organization[address]"
      assert_select "input#organization_contact_no[name=?]", "organization[contact_no]"
      assert_select "input#organization_additional_contact_no[name=?]", "organization[additional_contact_no]"
    end
  end
end
