require 'spec_helper'

describe "organizations/index" do
  before(:each) do
    assign(:organizations, [
      stub_model(Organization,
        :registration_no => 1,
        :owner_name => "Owner Name",
        :organization_name => "Organization Name",
        :address => "MyText",
        :contact_no => 2,
        :additional_contact_no => 3
      ),
      stub_model(Organization,
        :registration_no => 1,
        :owner_name => "Owner Name",
        :organization_name => "Organization Name",
        :address => "MyText",
        :contact_no => 2,
        :additional_contact_no => 3
      )
    ])
  end

  it "renders a list of organizations" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => "Owner Name".to_s, :count => 2
    assert_select "tr>td", :text => "Organization Name".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => 3.to_s, :count => 2
  end
end
