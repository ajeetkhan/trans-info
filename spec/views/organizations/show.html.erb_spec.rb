require 'spec_helper'

describe "organizations/show" do
  before(:each) do
    @organization = assign(:organization, stub_model(Organization,
      :registration_no => 1,
      :owner_name => "Owner Name",
      :organization_name => "Organization Name",
      :address => "MyText",
      :contact_no => 2,
      :additional_contact_no => 3
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/1/)
    rendered.should match(/Owner Name/)
    rendered.should match(/Organization Name/)
    rendered.should match(/MyText/)
    rendered.should match(/2/)
    rendered.should match(/3/)
  end
end
