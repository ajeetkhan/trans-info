require 'spec_helper'

describe "messages/edit" do
  before(:each) do
    @message = assign(:message, stub_model(Message,
      :message_text => "MyText",
      :sent_to => "MyString"
    ))
  end

  it "renders the edit message form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", message_path(@message), "post" do
      assert_select "textarea#message_message_text[name=?]", "message[message_text]"
      assert_select "input#message_sent_to[name=?]", "message[sent_to]"
    end
  end
end
