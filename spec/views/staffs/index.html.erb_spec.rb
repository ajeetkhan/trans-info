require 'spec_helper'

describe "staffs/index" do
  before(:each) do
    assign(:staffs, [
      stub_model(Staff,
        :name => "Name",
        :designation => "Designation",
        :address => "MyText",
        :contact_no => "Contact No",
        :email => "Email",
        :staff_type => "Staff Type"
      ),
      stub_model(Staff,
        :name => "Name",
        :designation => "Designation",
        :address => "MyText",
        :contact_no => "Contact No",
        :email => "Email",
        :staff_type => "Staff Type"
      )
    ])
  end

  it "renders a list of staffs" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "Designation".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => "Contact No".to_s, :count => 2
    assert_select "tr>td", :text => "Email".to_s, :count => 2
    assert_select "tr>td", :text => "Staff Type".to_s, :count => 2
  end
end
