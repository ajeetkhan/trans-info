require 'spec_helper'

describe "staffs/show" do
  before(:each) do
    @staff = assign(:staff, stub_model(Staff,
      :name => "Name",
      :designation => "Designation",
      :address => "MyText",
      :contact_no => "Contact No",
      :email => "Email",
      :staff_type => "Staff Type"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Name/)
    rendered.should match(/Designation/)
    rendered.should match(/MyText/)
    rendered.should match(/Contact No/)
    rendered.should match(/Email/)
    rendered.should match(/Staff Type/)
  end
end
