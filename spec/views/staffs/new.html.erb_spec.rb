require 'spec_helper'

describe "staffs/new" do
  before(:each) do
    assign(:staff, stub_model(Staff,
      :name => "MyString",
      :designation => "MyString",
      :address => "MyText",
      :contact_no => "MyString",
      :email => "MyString",
      :staff_type => "MyString"
    ).as_new_record)
  end

  it "renders new staff form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", staffs_path, "post" do
      assert_select "input#staff_name[name=?]", "staff[name]"
      assert_select "input#staff_designation[name=?]", "staff[designation]"
      assert_select "textarea#staff_address[name=?]", "staff[address]"
      assert_select "input#staff_contact_no[name=?]", "staff[contact_no]"
      assert_select "input#staff_email[name=?]", "staff[email]"
      assert_select "input#staff_staff_type[name=?]", "staff[staff_type]"
    end
  end
end
