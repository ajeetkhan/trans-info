FactoryGirl.define do
  factory :student do
    enrollment_no "MyString"
roll_no "MyString"
address "MyText"
father_name "MyString"
mother_name "MyString"
parents_contact_no "MyString"
additional_contact_no "MyString"
parents_email "MyString"
student_contact_no "MyString"
alumni false
  end

end
